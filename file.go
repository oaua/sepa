package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"time"
)

func getFilesFromArgs() []string {

	// Convert argument to absolute path
	absFilePaths := []string{}

	for _, path := range os.Args[1:] {

		// get absolute path
		absPath, err := filepath.Abs(path)
		if err != nil {
			log.Fatalln(err.Error())
		}

		absFilePaths = append(absFilePaths, absPath)
	}

	return absFilePaths
}

func getSubDirFilePath(subdirName string, absFile string) string {
	dir, fileName := filepath.Split(absFile)
	return filepath.Join(dir, subdirName, fileName)
}

func addDateFilePath(baseName string, date string) string {
	withoutExt := baseName[:len(baseName)-4]
	regex := regexp.MustCompile(`T[^T]+$`)
	date = regex.ReplaceAllString(date, "")
	return withoutExt + "--" + date + ".xml"
}

func ErrorFile(errToSave error, relatedFile string) {
	baseDir := filepath.Dir(relatedFile)
	errorFilePath := filepath.Join(baseDir, "errors.txt")

	file, err := os.OpenFile(
		errorFilePath,
		os.O_APPEND|os.O_CREATE|os.O_WRONLY,
		0664,
	)
	defer file.Close()

	if err != nil {
		log.Fatalln(err.Error())
	}

	message := fmt.Sprintf("\n ---- [%s] ----\nFILE: %s\nERROR: %s\n", time.Now(), relatedFile, errToSave.Error())
	if _, err := file.WriteString(message); err != nil {
		log.Fatalln(err.Error())
	}
}
