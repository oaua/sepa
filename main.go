package main

import (
	"log"
	"regexp"
	"sepa/model"
	"strings"
)

func main() {

	files := getFilesFromArgs()

	for _, file := range files {

		// read file
		sepaDocs, err := ReadSepaXmlMultiFile(file)
		// sepaDoc, err := ReadSepaXmlFile(file)
		if err != nil {
			ErrorFile(err, file)
			log.Fatalln(err.Error())
		} else {
			log.Printf("Read file: %v (%v)", file, len(sepaDocs.Documents))
		}

		// // save as it, with no change, for testin purpose
		// unchangedFile := getSubDirFilePath("unchanged", file)

		// if err := WriteSepaXmlFile(unchangedFile, sepaDoc); err != nil {
		// 	log.Fatalln(err.Error())
		// } else {
		// 	log.Printf("Write file: %v", unchangedFile)
		// }

		for _, doc := range sepaDocs.Documents {
			changedFile := getSubDirFilePath("changed", file)
			changedFile = addDateFilePath(changedFile, doc.BankToCustomerStatement.GroupHeader.CreationDateTime)
			changedSepaDoc := setInformation(doc)

			// save as it
			if err := WriteSepaXmlFile(changedFile, changedSepaDoc); err != nil {
				ErrorFile(err, file)
				log.Fatalln(err.Error())
			} else {
				log.Printf("Write file: %v", changedFile)
			}
		}
	}
}

func setInformation(doc model.Document) model.Document {
	for index, entry := range doc.BankToCustomerStatement.Statement.Entries {

		// get the information
		var information string

		switch entry.CreditDebitIndicator {
		case "CRDT":
			if reference := entry.EntryDetails.TransactionDetails.References; reference != nil && reference.MandateIdentification != nil {
				information = *reference.MandateIdentification
				doc.BankToCustomerStatement.Statement.Entries[index].EntryDetails.TransactionDetails.References.EndToEndId = reference.EndToEndId + "-" + information + "-" + *entry.AccountServicerReference
			} else if relatedParties := entry.EntryDetails.TransactionDetails.RelatedParties; relatedParties != nil && relatedParties.Debtor != nil {
				information = relatedParties.Debtor.Name
			} else {
				information = entry.EntryDetails.TransactionDetails.AdditionalTransactionInformation
			}
		case "DBIT":
			information = entry.EntryDetails.TransactionDetails.AdditionalTransactionInformation
		default:
			log.Fatalf("BankToCustomerStatement.Statement.Entries[%v].CreditDebitIndicator : has a invalid value %v", index, entry.CreditDebitIndicator)
		}

		// format information
		information = strings.Trim(information, " ")
		regex := regexp.MustCompile(`\s+`)
		information = regex.ReplaceAllString(information, " ")

		// set remittance information
		if entry.EntryDetails.TransactionDetails.RemittanceInformation == nil {
			remittanceInfo := model.RemittanceInformation{Unstructured: information}
			doc.BankToCustomerStatement.Statement.Entries[index].EntryDetails.TransactionDetails.RemittanceInformation = &remittanceInfo
		} else {
			entry.EntryDetails.TransactionDetails.RemittanceInformation.Unstructured = information
		}

	}
	return doc
}
