package main

import (
	"bytes"
	"encoding/xml"
	"os"
	"path/filepath"
	"sepa/model"
)

func ReadSepaXmlMultiFile(path string) (model.Multidocument, error) {
	// read file
	file_b, err := os.ReadFile(path)
	if err != nil {
		return model.Multidocument{}, err
	}

	// parse XML
	var parsedXml model.Multidocument

	file_b = bytes.Join([][]byte{[]byte("<MultiDoc>"), file_b, []byte("</MultiDoc>")}, []byte{})

	if err := xml.Unmarshal(file_b, &parsedXml); err != nil {
		return parsedXml, err
	}

	return parsedXml, nil
}

func ReadSepaXmlFile(path string) (model.Document, error) {
	// read file
	file_b, err := os.ReadFile(path)
	if err != nil {
		return model.Document{}, err
	}

	// parse XML
	var parsedXml model.Document

	if err := xml.Unmarshal(file_b, &parsedXml); err != nil {
		return parsedXml, err
	}

	return parsedXml, nil
}

func WriteSepaXmlFile(path string, sepaDoc model.Document) error {
	// create directory if not exists
	dir := filepath.Dir(path)

	// syscall.Umask(0)

	if err := os.MkdirAll(dir, 0764); err != nil {
		return err
	}

	// stringify in XML
	xml_b, err := xml.MarshalIndent(sepaDoc, "", "  ")
	if err != nil {
		return err
	}

	// add xml header
	doc_b := append([]byte(model.Header), xml_b...)
	// Save file
	if err := os.WriteFile(path, doc_b, 0664); err != nil {
		return err
	}

	return nil
}
