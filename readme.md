# SEPA CONVERTER
Modify SEPA XML export to be properly importable in Microsoft Business central.  

## what is modified ?
This SEPA field is use by Ms Business central as description :
```
Document / BankToCustomerStatement / Statement/ Entries / EntryDetails / TransactionDetails / RemittanceInformation / Unstructured
```
this converter defined or change the value of this field.

## where the new value from

when 
```
Document / BankToCustomerStatement / Statement/ Entries / CreditDebitIndicator
```
is equal to `CRDT`.

Value is from :
```
Document / BankToCustomerStatement / Statement/ Entries / EntryDetails / TransactionDetails.RelatedParties / Debtor / Name
```
If undefined or 
```
Document / BankToCustomerStatement / Statement/ Entries / CreditDebitIndicator
```
is equal to `DBIT`.
then the value is from : 
```
Document / BankToCustomerStatement / Statement/ Entries / EntryDetails /TransactionDetails / AdditionalTransactionInformation
```

## How to use 

Pass files as argument. (On windows, just dropdown file on .exe file).
Converted files will be write in a directory named `changed`.

#### Example :  
source file : `path/to/source/file/source.xml`  
converted file : `path/to/source/file/changed/source.xml`  
error file : `path/to/source/file/error.txt`


# Build

```bash
# build windows
GOOS=windows GOARCH=amd64 go build . 
# build linux
GOOS=linux GOARCH=amd64 go build . 
```
