package model

import (
	"encoding/xml"
	"log"
	"time"
)

const timeFormat = "2006-01-02T15:04:05"

type CreationDateTime time.Time

func (cdt *CreationDateTime) UnmarshalXML(d *xml.Decoder, element xml.StartElement) error {
	// Get raw value
	var rawString string
	if err := d.DecodeElement(&rawString, &element); err != nil {
		return err
	}

	// parse time
	if parsedTime, err := time.Parse(timeFormat, rawString); err != nil {
		log.Printf("BAD TIME FORMAT, error: %T %s", err, err.Error())
		return err
	} else {
		*cdt = CreationDateTime(parsedTime)
	}

	return nil
}

func (cdt CreationDateTime) String() string {
	time := time.Time(cdt)
	return time.Format(timeFormat)
}

func (cdt CreationDateTime) MarshalXML(e *xml.Encoder, element xml.StartElement) error {
	formatedDate := cdt.String()

	if err := e.EncodeElement(formatedDate, element); err != nil {
		return err
	}

	return nil
}
