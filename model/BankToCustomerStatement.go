package model

type BankToCustomerStatement struct {
	GroupHeader GroupHeader `xml:"GrpHdr"`
	Statement   Statement   `xml:"Stmt"`
}
