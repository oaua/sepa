package model

type Id struct {
	IBAN                       *string         `xml:"IBAN"`
	PrivateIdentification      *Identification `xml:"PrvtId"`
	OrganisationIdentification *Identification `xml:"OrgId"`
}
