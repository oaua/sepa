package model

const Header = `<?xml version="1.0" encoding="UTF-8"?>` + "\n"

type Document struct {
	Xmlns                   string                  `xml:"xmlns,attr"`
	BankToCustomerStatement BankToCustomerStatement `xml:"BkToCstmrStmt"`
}
