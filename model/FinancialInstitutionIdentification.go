package model

type FinancialInstitutionIdentification struct {
	BIC string `xml:"BIC"`
}
