package model

type RelatedParties struct {
	UltimateDebtor   *Debtor   `xml:"UltmtDbtr"`
	Debtor           *Debtor   `xml:"Dbtr"`
	Creditor         *Creditor `xml:"Cdtr"`
	UltimateCreditor *Creditor `xml:"UltmtCdtr"`
}
