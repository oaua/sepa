package model

type TransactionsSummary struct {
	TotalEntries       TotalEntries `xml:"TtlNtries"`
	TotalCreditEntries TotalEntries `xml:"TtlCdtNtries"`
	TotalDebitEntries  TotalEntries `xml:"TtlDbtNtries"`
}
