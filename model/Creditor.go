package model

type Creditor struct {
	Name *string `xml:"Nm"`
	Id   Id      `xml:"Id"`
}
