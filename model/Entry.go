package model

type Entry struct {
	Amount                     Amount              `xml:"Amt"`
	CreditDebitIndicator       string              `xml:"CdtDbtInd"`
	Status                     string              `xml:"Sts"`
	BookingDate                DateContainer       `xml:"BookgDt"`
	ValueDate                  DateContainer       `xml:"ValDt"`
	AccountServicerReference   *string             `xml:"AcctSvcrRef"`
	BankTransactionCode        BankTransactionCode `xml:"BkTxCd"`
	EntryDetails               EntryDetails        `xml:"NtryDtls"`
	AdditionalEntryInformation string              `xml:"AddtlNtryInf"`
}
