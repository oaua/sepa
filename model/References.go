package model

type References struct {
	EndToEndId            string  `xml:"EndToEndId"`
	MandateIdentification *string `xml:"MndtId"`
}
