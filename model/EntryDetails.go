package model

type EntryDetails struct {
	Batch *Batch `xml:"Btch"`

	TransactionDetails TransactionDetails `xml:"TxDtls"`
}
