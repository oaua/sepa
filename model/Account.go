package model

type Account struct {
	Id       Id       `xml:"Id"`
	Currency string   `xml:"Ccy"`
	Name     string   `xml:"Nm"`
	Servicer Servicer `xml:"Svcr"`
}
