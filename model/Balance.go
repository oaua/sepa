package model

type Balance struct {
	Type                 Type          `xml:"Tp"`
	Amount               Amount        `xml:"Amt"`
	CreditDebitIndicator string        `xml:"CdtDbtInd"`
	Date                 DateContainer `xml:"Dt"`
}
