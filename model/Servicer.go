package model

type Servicer struct {
	FinancialInstitutionIdentification FinancialInstitutionIdentification `xml:"FinInstnId"`
}
