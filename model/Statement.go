package model

type Statement struct {
	Id                       string              `xml:"Id"`
	ElectronicSequenceNumber string              `xml:"ElctrncSeqNb"`
	CreationDateTime         CreationDateTime    `xml:"CreDtTm"`
	Account                  Account             `xml:"Acct"`
	Balances                 []Balance           `xml:"Bal"`
	TransactionsSummary      TransactionsSummary `xml:"TxsSummry"`
	Entries                  []Entry             `xml:"Ntry"`
}
