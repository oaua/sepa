package model

type BankTransactionCode struct {
	Domain      Domain      `xml:"Domn"`
	Proprietary Proprietary `xml:"Prtry"`
}
