package model

type TotalEntries struct {
	NumberOfEntries      *int    `xml:"NbOfNtries"`
	Sum                  *string `xml:"Sum"`
	TotalNetEntryAmount  *string `xml:"TtlNetNtryAmt"`
	CreditDebitIndicator *string `xml:"CdtDbtInd"`
}
