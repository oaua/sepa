package model

type Proprietary struct {
	Code   string `xml:"Cd"`
	Issuer string `xml:"Issr"`
}
