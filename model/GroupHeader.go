package model

type GroupHeader struct {
	MessageIdentification string `xml:"MsgId"`
	CreationDateTime      string `xml:"CreDtTm"`
}
