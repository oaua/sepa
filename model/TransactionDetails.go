package model

type TransactionDetails struct {
	References                       *References            `xml:"Refs"`
	RelatedParties                   *RelatedParties        `xml:"RltdPties"`
	Purpose                          *Purpose               `xml:"Purp"`
	RemittanceInformation            *RemittanceInformation `xml:"RmtInf"`
	AdditionalTransactionInformation string                 `xml:"AddtlTxInf"`
}
