package model

type Purpose struct {
	Code        *string `xml:"Cd"`
	Proprietary *string `xml:"Prtry"`
}
