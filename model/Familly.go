package model

type Familly struct {
	Code           string `xml:"Cd"`
	SubFamillyCode string `xml:"SubFmlyCd"`
}
