package model

import (
	"encoding/xml"
	"time"
)

const dateFormat = "2006-01-02"

type Date time.Time

func (cdt *Date) UnmarshalXML(d *xml.Decoder, element xml.StartElement) error {
	// Get raw value
	var rawString string
	if err := d.DecodeElement(&rawString, &element); err != nil {
		return err
	}

	// parse time
	if parsedTime, err := time.Parse(dateFormat, rawString); err != nil {
		return err
	} else {
		*cdt = Date(parsedTime)
	}

	return nil
}

func (cdt Date) String() string {
	time := time.Time(cdt)
	return time.Format(dateFormat)
}

func (cdt Date) MarshalXML(e *xml.Encoder, element xml.StartElement) error {
	formatedDate := cdt.String()

	if err := e.EncodeElement(formatedDate, element); err != nil {
		return err
	}

	return nil
}
